source ~/.config/nvim/plug-config/coc.vim
set nocompatible            " disable compatibility to old-time vi
set showmatch               " show matching 
set ignorecase              " case insensitive 
set mouse=v                 " middle-click paste with 
set hlsearch                " highlight search 
set incsearch               " incremental search
set tabstop=4               " number of columns occupied by a tab 
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set number                  " add line numbers
set wildmode=longest,list   " get bash-like tab completions
set cc=80                  " set an 80 column border for good coding style          
filetype plugin indent on   "allow auto-indenting depending on file type
syntax on                   " syntax highlighting
set mouse=a                 " enable mouse click
set clipboard=unnamedplus   " using system clipboard
filetype plugin on
set cursorline              " highlight current cursorline
set ttyfast                 " Speed up scrolling in Vim
set spell                 " enable spell check (may need to download language package)
set noswapfile            " disable creating swap file
set splitright
set splitbelow

call plug#begin()
Plug 'w0rp/ale'
Plug 'junegunn/fzf'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'morhetz/gruvbox'
Plug 'voldikss/vim-floaterm'
Plug 'bling/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'mhinz/vim-startify'
Plug 'voldikss/vim-floaterm'
Plug 'tpope/vim-commentary'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/nerd-fonts'
call plug#end()

 if (has("termgu8colors"))
 set termguicolors
 endif
 syntax enable
 colorscheme gruvbox

let g:airline#extensions#tabline#enabled = 1
let g:python3_host_prog = '/usr/lib/python3.9'
let g:python_host_prog = '/usr/lib/python2.7'
let g:gruvbox_transparent_bg = 1

"NERDTree
nnoremap <A-n> :NERDTreeToggle<CR>
inoremap <A-n> <esc> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen = 1

inoremap <C-q><esc> :wq<CR>
nnoremap <C-q> :q<CR>

nnoremap <C-s> :w <CR>
inoremap <C-s> <esc> :w<CR>i

let g:ale_linters = {
    \ 'python': ['pyright'],
    \ 'vim': ['vint'],
    \ 'cpp': ['clangd'],
    \ 'c': ['clangd']
\}

inoremap <silent> <A-t><esc> :FloatermToggle<CR>i
inoremap <silent> <A-t> :FloatermToggle<CR>
let g:floaterm_autoclose=1
let g:floaterm_wintype='float'
let g:floaterm_position='center'
"copy
inoremap <C-p><esc> y <CR>i
nnoremap <C-p> y <CR>i
nnoremap <C-n> :badd
"paste
inoremap <C-v> <esc> p<CR>i
nnoremap <C-v>p<CR>


"navigate through buffers
let mapleader = '\<Space>'

nnoremap n  :bnext <CR>
nnoremap p :bprevious<CR>
nnoremap <silent> <C-/> <esc> gcc<CR>
nnoremap <silent> <C-/> gcc<CR>


let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }


"auto commands
autocmd FileType apache setlocal commentstring=#\ %s
